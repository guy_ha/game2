﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace Game1
{
    
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        CelAnimationManager celAnimationManger;
        ScrollingBackgroundManager scrollingBackgroundManger;

        private InputHandler inputHandler;

        private Player player;

        Controller arr;

        Texture2D ob;

        SpriteFont gameFont;
        SpriteFont timerFont;
        string player_pos;

        string hit = "Game Over";
        bool isHit = false;
        

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 600;

            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManger = new CelAnimationManager(this,"Textures\\");
            Components.Add(celAnimationManger);

            scrollingBackgroundManger = new ScrollingBackgroundManager(this, "Textures\\");
            Components.Add(scrollingBackgroundManger);

            player = new Player(this);
            Components.Add(player);

            arr = new Controller(this);

        }

     
        protected override void Initialize()
        {
            
            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            player.Load(spriteBatch);

            gameFont = Content.Load<SpriteFont>("spaceFont");
            timerFont = Content.Load<SpriteFont>("timerFont");

            ob = Content.Load<Texture2D>("obs1");

            scrollingBackgroundManger.AddBackground("Bg", "Background", new Vector2(0, 0), new Rectangle(0, 0, 1600, 600), 10, 0.5f, Color.White);
            scrollingBackgroundManger.AddBackground("Floor", "Grass", new Vector2(0, 540), new Rectangle(0, 0, 1600, 74), 100, 0.1f, Color.White);
            
        }

       
        protected override void UnloadContent()
         {
         }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right) && player.Position.X >= (graphics.GraphicsDevice.Viewport.Width / 2.0f))
                scrollingBackgroundManger.ScrollRate = -2.0f;
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left) && player.Position.X <= 0.0f)
                scrollingBackgroundManger.ScrollRate = 2.0f;
            else
                scrollingBackgroundManger.ScrollRate = 0.0f;

            //if (Vector2.Distance(player.Position, new Vector2(300, 400)) < 100)
               // isHit = true;
             player_pos = player.Position.ToString();

            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.BackToFront);

            scrollingBackgroundManger.Draw("Bg", spriteBatch);
            scrollingBackgroundManger.Draw("Floor", spriteBatch);
            spriteBatch.Draw(ob, new Vector2(300,400),Color.White);
            if (isHit)
            { 
            spriteBatch.DrawString(gameFont, hit, new Vector2(400, 400), Color.White);
            }
            spriteBatch.DrawString(gameFont, player_pos, new Vector2(600, 400), Color.White);

            base.Draw(gameTime);

            spriteBatch.End();
        }
    }
}
