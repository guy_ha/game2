﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace Game1
{
    class Controller: Microsoft.Xna.Framework.DrawableGameComponent
    {
        Texture2D obsTex;
        static int LINE_WIDTH = 530;
        // public List<Obstacle> ObsList = new List<Obstacle>();
        private Obstacle[] obsArr = new Obstacle[10];
        Random rnd = new Random();
        SpriteBatch batch;

        public Controller(Game game)
         : base(game)
        {   
        }
        public override void Initialize()
        {
            for(int i=0; i<10; i++)
            {
               obsArr[i] = new Obstacle(new Vector2(rnd.Next(1000), LINE_WIDTH)) ;
            }
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
             obsTex = Game.Content.Load<Texture2D>("Textures/spike");
             batch = new SpriteBatch(Game.GraphicsDevice);
        }
        public override void Draw(GameTime gameTime)
        {
            batch.Begin(SpriteSortMode.Deferred,BlendState.NonPremultiplied);
            batch.Draw(obsTex, new Vector2(300, 500), Color.White);
            batch.End();    

        }
    }
}
