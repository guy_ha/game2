﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;



namespace Game1
{
   public class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {


        public float timer = 0f;
        private int jumpSumX = 0;
        private int jumpSumY = 0;
        bool timePass = false;
        bool down = false;

        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;

        SpriteBatch spriteBatch;

        private Vector2 position = new Vector2(0,500);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        enum Direction { Left,Right};

        private Direction direction = Direction.Right;
        private float moveRate = 150.0f;

        public Player(Game game)
            : base(game)
        {
            
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
        }

        public bool timeElapsed1(float currentTime)
        {
            float timer2 = timer - currentTime;
            if (timer2 > 2f)
                timePass = true;
            return timePass;
        }

        public void jumpUp (GameTime gameTime)
        {
           float sum =(float) gameTime.ElapsedGameTime.TotalSeconds;
            for (int i =0; i<10; i++)
            {
                position.X += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
                position.Y -= (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            for (int i = 0; i < 5; i++)
            {
                position.X -= (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
                position.Y += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            if((sum-(float)gameTime.ElapsedGameTime.TotalSeconds)<-2)
            jumpDown(gameTime);
        }
        public void jumpDown(GameTime gameTime)
        {
            for (int i = 0; i < 10; i++)
            {
                position.X -= (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
                position.Y += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
           
        }

        protected override void LoadContent()
        {
            CelCount celCount = new CelCount(5, 1);
            celAnimationManager.AddAnimation("TestAnimation", "hammer", celCount, 10);
            celAnimationManager.ToggleAnimation("TestAnimation");

            CelCount celCountHit = new CelCount(5, 1);
            celAnimationManager.AddAnimation("Hit1", "hit", celCount,10);
            celAnimationManager.ToggleAnimation("Hit1");

            CelCount celCountJump = new CelCount(5, 1);
            celAnimationManager.AddAnimation("jj", "hammer", celCount, 500);
            celAnimationManager.ToggleAnimation("jj");

        }
        public override void Update(GameTime gameTime)
        {
            timer = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                celAnimationManager.ResumeAnimation("TestAnimation");
                direction = Direction.Right;
                position.X += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                celAnimationManager.ResumeAnimation("TestAnimation");
                direction = Direction.Left;
                position.X -= (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Space))
            {
                celAnimationManager.ResumeAnimation("Hit1");
                direction = Direction.Right;
                position.X += (moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds);
            }
            else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Enter))
            {
              
              
                celAnimationManager.ResumeAnimation("jj");
                direction = Direction.Right;
               
                jumpUp(gameTime);

               

              
            }
            else
                celAnimationManager.PauseAnimation("TestAnimation");

            int celWidth = celAnimationManager.GetAnimationFrameWidth("TestAnimation");

            if (position.X > (Game.GraphicsDevice.Viewport.Width * 0.5f))
                position.X = (Game.GraphicsDevice.Viewport.Width * 0.5f);
            if (position.X < 0)
                position.X = 0;

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Space))
            {
                celAnimationManager.Draw(gameTime, "Hit1", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.None);
            }
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Enter))
            {
                celAnimationManager.Draw(gameTime, "jj", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.None);
            }
            else
            celAnimationManager.Draw(gameTime, "TestAnimation", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
           
        }

    }
}
